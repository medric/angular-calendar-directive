/* global angular */
/* global escape */
'use strict'; // jshint ignore:line

var app = angular.module('app', [
    'ngRoute',
    'gp.calendar'
]);

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $routeProvider
        .when('/angular-test-calendar', {
            templateUrl: 'angular-test-calendar/views/demo-calendar.html'
        });
        //.otherwise({redirectTo: '/angular-test-calendar'});
});

app.controller('AppController', function($scope /*,Service1, Service2 ...*/) {
    $scope.resources = [];
    $scope.view = 'classic';
    var foo = {
      id: 1,
      name: 'foo',
      assignments: {
        event1: {
          name: 'project1',
          date: moment()
        }
      }
    },
    bar = {
      id: 2,
      name: 'bar',
      assignments: {
        event1: {
          name: 'project1',
          date: moment()
        }
      }
    };

    $scope.resources.push(foo);
    $scope.resources.push(bar);
});
