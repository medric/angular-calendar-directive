!function() {
/* global angular */
'use strict'; // jshint ignore:line

angular.module('gp.calendar', [])
	.service('gpCalendarService', function(){
		// PUBLIC API
		var api = {};

		//$window.navigator.language !== null ? $window.navigator.language : $window.navigator.browserLanguage,
		api.locale = 'fr';
		api.now = moment().locale(api.locale);
		api.localData = api.now.localeData();

		api.sortedDays = function() {
			var daysOfWeek = api.localData._weekdays;

			// Add Sunday to the end
			daysOfWeek.push(daysOfWeek[0]);
			// Remove it from the beginning
			daysOfWeek.splice(0, 1);

			return daysOfWeek;
		}

		api.startOfMonth = function() {
			return moment(api.now).startOf('month');
		}

		api.generateCalendar = function(startingDate, endingDate, selectedDate) {
			var days = [];

			var day = undefined;
			for(var date = startingDate; date.isBefore(endingDate); date.add(1, 'days')) {
				day = {
					date: moment(date.format()),
					formatted: date.format('ddd DD.MM'),
					isSelected: date.isSame(selectedDate),
					isEndOfWeek: date.isSame(moment(date).endOf('week'), 'day'),
					isWeekEnd: date.day() == 0 || date.day() == 6,
					isEndOfMonth: date.isSame(moment(date).endOf('month'), 'day'),
					month: date.month()
				}
				days.push(day);
			}

			return days;
		}

		return api;
	})
	.controller('CalendarController', ['$scope', '$window', '$compile', 'gpCalendarService', function($scope, $window, $compile, gpCalendarService) {

		var $element,
				service = gpCalendarService;

		this.init = function(element) {
			$scope.element = element;
			// init scope data
			$scope.now = service.now;
			$scope.localeData = $scope.now.localeData();
			$scope.month = $scope.month || service.startOfMonth();
			$scope.isWeekEndShown = true;

			// default selected date is set to today 12:00 am
			$scope.selectedDate = {
				date: moment($scope.now).startOf('day'),
				formatted: moment($scope.now).format('ddd DD')
			};

			generateCalendar();
		}

		// scope methods
		$scope.previousMonth = function() {
			$scope.month.subtract(1, 'month');
			generateCalendar();
		}

		$scope.nextMonth = function() {
			$scope.month.add(1, 'month');
			generateCalendar();
		}

		$scope.filterWeekEnds = function() {
			$scope.isWeekEndShown ? $scope.isWeekEndShown = false : $scope.isWeekEndShown = true;
		}

		/*
			events management - dates & resources
		*/
		$scope.selectionLocked = false;
		$scope.selectedDates = [];

		$scope.enter = function(e, resource) {
			if($(e.currentTarget).hasClass('selected')) {
				$(e.currentTarget).removeClass('selected');
				$(e.currentTarget).attr('data-selected', 'false');
			}
			else {
				// lock the row
				$scope.selectionLocked = true;
				setAsSelected($(e.currentTarget));
				$scope.selectedResource = resource.id;
			}
		}

		$scope.select = function(e, resource) {
			// has to be on the same row
			if($scope.selectionLocked && resource.id == $scope.selectedResource) {
				setAsSelected($(e.currentTarget));
			}
		}

		$scope.release = function(e) {
			$scope.selectionLocked = false;
			console.log($scope.selectedDates);
		}

		// private function
		function generateCalendar() {
			var firstDay = moment($scope.month),
					lastDay = moment(firstDay).endOf('month').add(2, 'months');

			$scope.days = service.generateCalendar(firstDay, lastDay, $scope.selectedDate.date);
		}

		function setAsSelected(element) {
			element.addClass('selected');
			element.attr('data-selected', 'true');
		}
	}])
	.directive('gpCalendar', function(){
		return {
			restrict: 'AE',
			controller: 'CalendarController',
			scope: {
				view: '@', // 1-way binding - ne modidifie pas la valeur dans le scope parent
				resources: '=' // 2 way data-binding - modifie la valeur dans le scope parent
			},
			templateUrl: 'angular-test-calendar/app/calendar/gp-calendar.html',
			link: function(scope, element, attrs, ctrl, compile) {
				ctrl.init(element);

				scope.$watch('currentRow', function() {

				});
			}
		}
	});
}();